import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InfiniteScroll } from 'ionic-angular/components/infinite-scroll/infinite-scroll';

import firebase from 'firebase';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  users: any[];
  usersSearch: any[];
  searchTerm: string;

  constructor(public navCtrl: NavController) {
    this.getUsers();
  }

  getUsers() {
    this.users = []
    firebase.database().ref('institutions').orderByChild('email').limitToFirst(12).on('value', snapshot => {
      snapshot.forEach(user => {
        this.users.push({
          key: snapshot.key,
          email: user.val().email
        })
      })
    })
  }

  doInfinite(infinity) {
    firebase.database().ref('institutions').equalTo('city', '').orderByChild('email').limitToFirst(12).startAt(this.users[this.users.length -1].email).on('value', snapshot => {
      snapshot.forEach(user => {
        if(user.val().email !== this.users[this.users.length -1].email) {
          this.users.push({
            key: snapshot.key,
            email: user.val().email
          })
        }
      })
      infinity.complete()
    })
  }

  searchUsers() {
    this.users = []
    firebase.database().ref('institutions').orderByChild('city').equalTo('Teresina').limitToFirst(12).on('value', snapshot => {
      console.log(snapshot.val())
      snapshot.forEach(user => {
        this.users.push({
          key: snapshot.key,
          email: user.val().email
        })
      })
    })
  }

  paginateSearch(infinity) {
    firebase.database().ref('institutions').orderByChild('city').equalTo('Teresina').limitToFirst(12).startAt(this.users[this.users.length -1].email).on('value', snapshot => {
      snapshot.forEach(user => {
        if(user.val().email !== this.users[this.users.length -1].email) {
          this.users.push({
            key: snapshot.key,
            email: user.val().email
          })
        }
      })
      infinity.complete()
    })
  }

}
